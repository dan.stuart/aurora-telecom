[![Netlify Status](https://api.netlify.com/api/v1/badges/bb5c0e88-4c6a-4bff-9708-f83168c03a0b/deploy-status)](https://app.netlify.com/sites/aurora-telecom/deploys)

# aurora-telecom

> My splendiferous Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

