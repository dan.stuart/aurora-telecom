module.exports = {
  "root": true,
  "env": {
    "node": true
  },
  "extends": [
    "plugin:vue/recommended",
    "eslint:recommended",
    "plugin:prettier/recommended",
    "prettier/vue"
  ],
  "rules": {
    "no-console": [
      "error",
      {
        "allow": [
          "log",
          "error",
          "warn"
        ]
      }
    ]
  },
  "globals": {
    "$nuxt": true
  },
  "parserOptions": {
    "parser": "babel-eslint"
  }
}